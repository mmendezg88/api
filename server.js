//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get('/',function(req, res){
    res.sendFile(path.join(__dirname,'index.html'));
    //JSON CON 3 CLIENTES FICTICIOS
});

app.post('/', function(req,res){
    res.send('Su peticion ha sido recibida');
    //Su cliente ha sido dado de alta
});

app.get('/clientes',function(req, res){
    res.sendFile(path.join(__dirname,'clientes.json'));
    //JSON CON 3 CLIENTES FICTICIOS
});

app.get('/clientes/:id',function(req,res) {
    res.send("aqui tiene el cliente numero: " + req.params.id);
});

app.post('/clientes', function(req,res){
    res.send('Sus clientes han sido dados de  Cambiado');
    //Su cliente ha sido dado de alta
});

app.put('/clientes',function (req,res){
    res.send('Su informacion ha sido actualizada');
});

app.delete('/clientes',function (req,res){
    res.send('El cliente ha sido Eliminado');
});
